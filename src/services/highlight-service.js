import Twingl from '../../lib/twingl';
import { IDBService } from '../services';

class HighlightService {
  static updateLocalCache(token) {
    return new Promise((resolve, reject) => {
      let client = new Twingl.Client({
        token,
        baseUrl: process.env.API_BASE_URL,
        version: process.env.API_VERSION
      });
      let db = IDBService.getInstance();

      client.highlights.index().then((highlights) => {
        db.transaction("rw", db.highlights, () => {
          highlights.map(h => db.highlights.add(h));
        })
        .catch(reject)
        .then(() => {
          db.close()
          resolve();
        });
      });
    });
  }
}

export { HighlightService };
export default HighlightService;
