class TokenService {

  static isTokenEvent(evt) {
    return (evt instanceof StorageEvent && evt.key === 'token');
  }

  static emitTokenEvent(emitter, token) {
    emitter.emit('token', { key: 'token', newValue: token });
  }

  static getToken() {
    return localStorage.getItem('token');
  }

  static setToken(token, expires, emitter) {
    localStorage.setItem('token', token);
    localStorage.setItem('expires', expires);

    if (emitter) TokenService.emitTokenEvent(emitter, token);
  }

  static setTokenFromJSO(token, emitter) {
    TokenService.setToken(token.access_token, token.expires, emitter);
    TokenService.getJSOInstance().wipeTokens();
  }

  static clearToken(emitter) {
    localStorage.removeItem('token');
    TokenService.getJSOInstance().wipeTokens();

    if (emitter) TokenService.emitTokenEvent(emitter, null);
  }

  static getJSOInstance() {
    return new JSO({ 
      providerID:     process.env.OAUTH_PROVIDER_ID,
      client_id:      process.env.OAUTH_CLIENT_ID,
      redirect_uri:   process.env.OAUTH_REDIRECT_URI,
      authorization:  process.env.OAUTH_AUTHORIZATION_URI
    });
  }

};

export default TokenService;
