import Base from './__base';

class Highlight extends Base {

  constructor(client, opts) {
    super(client, { resourceEndpoint: 'highlights', ...opts });
  }

};

export default Highlight;
