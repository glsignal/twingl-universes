import Base from './__base';

class Note extends Base {

  constructor(client, opts) {
    super(client, { resourceEndpoint: 'notes', ...opts });
  }

};

export default Note;
