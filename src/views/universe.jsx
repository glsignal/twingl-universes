import React from 'react';

class Universe extends React.Component {

  render() {
    return <p>Universe</p>;
  }

};

Universe.propTypes = {
  token: React.PropTypes.string.isRequired,
  events: React.PropTypes.object.isRequired
}

export default Universe;
