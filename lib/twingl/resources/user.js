import Base from './__base';

class User extends Base {

  constructor(client, opts) {
    super(client, { resourceEndpoint: 'users', ...opts });
  }

  me() {
    return this.read('me');
  }

};

export default User;
