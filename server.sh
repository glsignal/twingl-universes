#!/bin/bash
python_major=$(python -V 2>&1 | cut -d " " -f 2 | cut -d "." -f 1)
if [ $python_major -lt 3 ]; then
    python -m SimpleHTTPServer "$@"
else
    python -m http.server "$@"
fi
