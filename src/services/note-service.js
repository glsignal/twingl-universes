import Twingl from '../../lib/twingl';
import { IDBService } from '../services';

class NoteService {
  static updateLocalCache(token) {
    return new Promise((resolve, reject) => {
      let client = new Twingl.Client({
        token,
        baseUrl: process.env.API_BASE_URL,
        version: process.env.API_VERSION
      });
      let db = IDBService.getInstance();

      client.notes.index().then((notes) => {
        db.transaction("rw", db.notes, () => {
          notes.map(n => db.notes.add(n));
        })
        .catch(reject)
        .then(() => {
          db.close()
          resolve();
        });
      });
    });
  }
}

export { NoteService };
export default NoteService;
