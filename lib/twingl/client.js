import { User } from './resources'
import * as Resource from './resources';
import Base from './resources/__base';

class Client {

  constructor({ token, baseUrl, version }) {
    this.token = token;
    this.baseUrl = baseUrl;
    this.version = version;

    this.highlights = new Resource.Highlight(this);
    this.notes      = new Resource.Note(this);
    this.links      = new Resource.Link(this);
    this.contexts   = new Resource.Context(this);
  }

  me() {
    return new User(this).me();
  }

  search(term, query = {}) {
    query.q = term;
    return new Base(this, { resourceEndpoint: 'search' }).index(query);
  }

};

export default Client;
