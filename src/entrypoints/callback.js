import callback from '../markup/callback.html';

import { TokenService } from '../services';

var jso = TokenService.getJSOInstance();

jso
  .callback()
  .then((token) => {
    TokenService.setTokenFromJSO(token)
    window.location.href = '/';
  });
