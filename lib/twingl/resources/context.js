import Base from './__base';

class Context extends Base {

  constructor(client, opts) {
    super(client, { resourceEndpoint: 'contexts', ...opts });
  }

  /**
   * Get a list of all of the current user's contexts
   */
  mine(query) {
    let url = `${this.__baseUrl()}/${this.resourceEndpoint}/mine`;
    return this.__request(url, 'get', { query });
  }

};

export default Context;
