import React from 'react';

import { TokenService } from '../services';
import Twingl from '../../lib/twingl';

class Loading extends React.Component {

  constructor(props) {
    super(props);

    this.api = new Twingl.Client({
      token: TokenService.getToken(),
      baseUrl: process.env.API_BASE_URL,
      version: process.env.API_VERSION
    });
  }

  render() {
    return <div className='view--loading'>
    </div>;
  }

};

Loading.propTypes = {
  token: React.PropTypes.string.isRequired,
  events: React.PropTypes.object.isRequired
}

export default Loading;
