import React from 'react';

import { TokenService } from '../services';

class SignIn extends React.Component {

  constructor(props) {
    super(props);

    this.jso = TokenService.getJSOInstance();
  }

  onSignInClicked(evt) {
    evt.preventDefault();

    // Launch OAuth2 implicit flow
    this.jso.getToken({})
      .then((token) => {
        if (token) {
          TokenService.setTokenFromJSO(token, this.props.events);
        }
      });
  }

  render() {
    return <div className='view--sign-in'>
      <form>
        <input type='submit' onClick={this.onSignInClicked.bind(this)} value='Sign In' />
      </form>
    </div>;
  }

};

SignIn.propTypes = {
  events: React.PropTypes.object.isRequired
}

export default SignIn;
