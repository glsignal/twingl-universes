import Twingl from '../../lib/twingl';
import { IDBService } from '../services';

class ContextService {
  static updateLocalCache(token) {
    return new Promise((resolve, reject) => {
      let client = new Twingl.Client({
        token,
        baseUrl: process.env.API_BASE_URL,
        version: process.env.API_VERSION
      });
      let contexts = new Twingl.Resource.Context(client);
      let db = IDBService.getInstance();

      contexts.mine().then((contexts) => {
        db.transaction("rw", db.contexts, () => {
          contexts.map(c => db.contexts.add(c));
        })
        .catch(reject)
        .then(() => {
          db.close()
          resolve();
        });
      });
    });
  }
}

export { ContextService };
export default ContextService;
