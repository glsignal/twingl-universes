import Client from './twingl/client';

import * as Resource from './twingl/resources';

export default { Client, Resource };
