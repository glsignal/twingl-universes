import Twingl from '../../lib/twingl';
import { IDBService } from '../services';

class LinkService {
  static updateLocalCache(token) {
    return new Promise((resolve, reject) => {
      let client = new Twingl.Client({
        token,
        baseUrl: process.env.API_BASE_URL,
        version: process.env.API_VERSION
      });
      let db = IDBService.getInstance();

      client.links.index().then((links) => {
        db.transaction("rw", db.links, () => {
          links.map(l => db.links.add(l));
        })
        .catch(reject)
        .then(() => {
          db.close()
          resolve();
        });
      });
    });
  }
}

export { LinkService };
export default LinkService;
