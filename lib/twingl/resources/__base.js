import superagent from 'superagent';

/**
 * Resource base class. Should not be used directly, instead it should be
 * subclassed
 */
class Base {

  /**
   * `Client` instance holding authentication information
   */
  client;

  /**
   * The endpoint this resource is held under
   */
  resourceEndpoint;

  /**
   * Optional parent resource (e.g. Notes that belong to a Highlight)
   */
  parentResource;

  /**
   * Create a new Base resource
   */
  constructor(client, { resourceEndpoint, parentResource }) {
    this.client = client;
    this.resourceEndpoint = resourceEndpoint;
    this.parentResource = parentResource;
  }

  search(term, query = {}) {
    query.q = term;

    let url = `${this.__baseUrl()}/${this.resourceEndpoint}/search`;
    return this.__request(url, 'get', { query });
  }

  /**
   * Read any number of records
   */
  index(query = {}) {
    let url = `${this.__baseUrl(this.parentResource)}/${this.resourceEndpoint}`;
    return this.__request(url, 'get', { query });
  }

  /**
   * Read a specific record
   */
  read(id) {
    let url = `${this.__baseUrl()}/${this.resourceEndpoint}/${id}`;
    return this.__request(url, 'get');
  }

  /**
   * Create a new record
   */
  create(attributes) {
    let url = `${this.__baseUrl(this.parentResource)}/${this.resourceEndpoint}`;
    return this.__request(url, 'post', { attributes });
  }

  /**
   * Update an existing record
   */
  update(id, attributes) {
    let url = `${this.__baseUrl()}/${this.resourceEndpoint}/${id}`;
    return this.__request(url, 'put', { attributes });
  }

  /**
   * Destroy an existing record
   */
  destroy(id) {
    let url = `${this.__baseUrl()}/${this.resourceEndpoint}/${id}`;
    return this.__request(url, 'delete');
  }

  __baseUrl(parentResource) {
    if (parentResource && parentResource.id && parentResource.type) {
      return [
        this.client.baseUrl,
        parentResource.type,
        parentResource.id,
        this.client.version
      ].join('/');
    } else {
      return [this.client.baseUrl, this.client.version].join('/');
    }
  }

  __request(url, method, { data, query } = {}) {
    return new Promise((resolve, reject) => {
      var request = superagent(method, url)
        .set('Authorization', 'Bearer ' + this.client.token)
        .set('Accept', 'application/json');

      if (data) {
        request.set('Content-Type', 'application/json');
        request.send(data);
      }

      if (query) request.query(query);

      request.end((error, response) => {
        if (error) {
          //If there's an error present, bubble it up to the consumer
          reject(error);

        } else {
          if (response.statusType === 4 || response.statusType === 5) {
            reject(JSON.parse(response.text));

          } else if (response.status === 204) {
            resolve(response);

          } else {
            resolve(JSON.parse(response.text));
          }
        }
      });
    });
  }

};

export default Base;
