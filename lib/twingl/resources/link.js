import Base from './__base';

class Link extends Base {

  constructor(client, opts) {
    super(client, { resourceEndpoint: 'twinglings', ...opts });
  }

};

export default Link;
