import Dexie from 'dexie';

class IDBService {
  static getInstance() {
    let db = new Dexie("universes-database");
    db.version(1)
      .stores({
        highlights: 'id',
        notes:      'id',
        links:      'id',
        contexts:   'id'
      });
    db.open();
    return db;
  }
}

export { IDBService };
export default IDBService;
