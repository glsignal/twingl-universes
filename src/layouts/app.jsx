import React from 'react';

import { SignIn, Loading, Universe } from '../views';
import * as Service from '../services';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      token: Service.TokenService.getToken(),
      loading: true
    };
  }

  loadData() {
    console.log("load", Service);
    Promise.all([
        Service.ContextService.updateLocalCache(this.state.token),
        Service.NoteService.updateLocalCache(this.state.token),
        Service.HighlightService.updateLocalCache(this.state.token),
        Service.LinkService.updateLocalCache(this.state.token)
    ]).then((value) => {
      //INITIALIZE THE GRAPH
    }, console.log.bind(console))
    .catch(console.log.bind(console));
  }

  onTokenChange(evt) {
    this.setState({ token: evt.newValue });
  }

  componentDidMount() {
    this.__tokenListener = this.onTokenChange.bind(this);
    this.props.events.on('token', this.__tokenListener);

    this.loadData();
  }

  componentWillUnmount() {
    this.props.events.off('token', this.__tokenListener);
  }

  render() {
    if (this.state.token && !this.state.loading) {
      return <Universe token={this.state.token} events={this.props.events} />;
    } else if (this.state.token) {
      return <Loading token={this.state.token} events={this.props.events} />;
    } else {
      return <SignIn events={this.props.events} />;
    }
  }
};

App.propTypes = {
  events: React.PropTypes.object.isRequired
}

export default App;
