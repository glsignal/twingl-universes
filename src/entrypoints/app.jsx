import styles from '../styles/app.manifest.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import domready from 'domready';
import EventEmitter from 'event-emitter';

import { App } from '../layouts';
import { IDBService, TokenService } from '../services';

var emitter = EventEmitter({});

IDBService.getInstance();

window.addEventListener('storage', (evt) => {
  if (TokenService.isTokenEvent(evt)) {
    let { newValue } = evt;

    TokenService.emitTokenEvent(emitter, newValue);
  }
});

domready(() => {
  ReactDOM.render(<App events={emitter} />, document.getElementById('container'));
});
